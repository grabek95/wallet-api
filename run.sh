docker rm $(docker ps -a -q) -f #removes your containers
docker network prune -f #remove all unused networks
docker-compose up -d #running your containers
docker exec -it Wallet composer install
docker exec -it Wallet php bin/console --no-interaction doctrine:migrations:migrate
docker exec -it Wallet php bin/console --no-interaction doctrine:migrations:migrate --env=test
docker exec -it Wallet php bin/console cache:clear
docker exec -it Wallet php ./vendor/bin/phpunit

