<?php

namespace App\Service;

use App\Entity\WalletTransaction;
use App\Repository\WalletRepository;
use App\Repository\WalletTransactionRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;

class WalletTransactionService
{
    private $walletRepository;
    private $objectManager;

    public function __construct(WalletRepository $walletRepository, EntityManagerInterface $objectManager)
    {
        $this->walletRepository = $walletRepository;
        $this->objectManager = $objectManager;
    }

    public function addToWallet($walletId, $sum)
    {
        $wallet = $this->walletRepository->find($walletId);
        $transaction = new  WalletTransaction();
        $transaction->setWalletId($wallet);
        $transaction->setType('add');
        $transaction->setSum($sum);
        $transaction->setCreatedAt(new DateTimeImmutable());
        $newSum = $wallet->getSum() + $sum;
        $wallet->setSum($newSum);
        $wallet->setUpdatedAt(new DateTimeImmutable());
        $this->objectManager->persist($transaction);
        $this->objectManager->flush();

        return $wallet;
    }

    public function subFromWallet($walletId, $sum)
    {
        $wallet = $this->walletRepository->find($walletId);
        $transaction = new  WalletTransaction();
        $transaction->setType('sub');
        $transaction->setWalletId($wallet);
        $transaction->setSum($sum);
        $transaction->setCreatedAt(new DateTimeImmutable());
        $wallet->setSum($wallet->getSum() - $sum);
        $wallet->setUpdatedAt(new DateTimeImmutable());
        $this->objectManager->persist($transaction);
        $this->objectManager->flush();

        return $wallet;
    }
}