<?php

namespace App\Service;

use App\Entity\Wallet;
use App\Repository\WalletRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;

class WalletService
{
    private $walletRepository;

    public function __construct(WalletRepository $walletRepository)
    {
        $this->walletRepository = $walletRepository;
    }

    public function create($sum)
    {
        return $this->walletRepository->create($sum);
    }

    public function getSumByWalletId($id)
    {
        return $this->walletRepository->find($id);
    }

    public function getWalletWithHistoryCsvResponse($walletId)
    {
        $result = [];
        $wallet = $this->walletRepository->find($walletId);
        if ($wallet) {
            $walletHistory = $wallet->getWalletTransactions();
        }
        $response = new StreamedResponse();
        $response->setCallback(
            function () use ($wallet, $walletHistory) {
                $handle = fopen('php://output', 'r+');
                fputcsv($handle, ['Wallet id', $wallet->getId(), 'Balance', $wallet->getSum()], ';');
                fputcsv($handle, ['', '', '', ''], ';');
                $columns = ['type', 'sum', 'date'];
                fputcsv($handle, $columns, ';');
                foreach ($walletHistory as $row) {
                    $data = array(
                        $row->getType(),
                        $row->getSum(),
                        $row->getCreatedAt()->format('Y-m-d H:i:s'),
                    );
                    fputcsv($handle, $data, ';');
                }
                fclose($handle);
            }
        );
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

        return $response;
    }

}