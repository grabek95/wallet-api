<?php

namespace App\Controller\Api;

use App\Service\WalletTransactionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WalletTransactionController extends AbstractController
{
    private $walletTransactionService;

    public function __construct(WalletTransactionService $walletTransactionService)
    {
        $this->walletTransactionService = $walletTransactionService;
    }

    /**
     * @Route("/api/walletTransaction/addToWallet", name="apiAddToWallet", methods={"POST"})
     */
    public function addToWallet(Request $request): Response
    {
        $id = $request->get('id');
        $sum = $request->get('sum');
        $result = $this->walletTransactionService->addToWallet($id, $sum);
        $result = $result ? $result->toArray() : null;
        return new JsonResponse($result, $result ? Response::HTTP_OK : Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/api/walletTransaction/subFromWallet", name="apiSubFromWallet", methods={"POST"})
     */
    public function subFromWallet(Request $request): Response
    {
        $id = $request->get('id');
        $sum = $request->get('sum');
        $result = $this->walletTransactionService->subFromWallet($id, $sum);
        $result = $result ? $result->toArray() : null;
        return new JsonResponse($result, $result ? Response::HTTP_OK : Response::HTTP_NOT_FOUND);
    }
}
