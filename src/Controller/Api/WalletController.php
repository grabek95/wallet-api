<?php

namespace App\Controller\Api;

use App\Service\WalletService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WalletController extends AbstractController
{
    private $walletService;

    public function __construct(WalletService $walletService)
    {
        $this->walletService = $walletService;
    }

    /**
     * @Route("/api/wallet", name="apiCreateWallet", methods={"POST"})
     */
    public function create(Request $request): Response
    {
        $wallet = $this->walletService->create($request->get('sum'));
        $wallet = $wallet ? $wallet->toArray() : null;
        return new JsonResponse($wallet, $wallet ? Response::HTTP_OK : Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/api/wallet/balance/{id}", name="apiGetBalanceForWallet", methods={"GET"})
     */
    public function getBalanceById($id): Response
    {
        $wallet = $this->walletService->getSumByWalletId($id);
        $wallet = $wallet ? $wallet->toArray() : null;
        return new JsonResponse($wallet, $wallet ? Response::HTTP_OK : Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/api/wallet/history/{id}", name="apiGetWalletHistory", methods={"GET"})
     */
    public function getWalletWithHistoryCsv($id): Response
    {
        return $this->walletService->getWalletWithHistoryCsvResponse($id);
    }
}
