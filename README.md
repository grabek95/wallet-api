## Instrukcja uruchomienia projektu

Aby uruchomic project należy:

- posiadac zainstalowanego dockera i docker-compose
- wpliku .env ustawion sa porty zewnetrzne dla aplikacji domyslnie aplikacja jest postawiona na porcie 8123
- odpalic skrypt docker.sh
- przy kolejnych odpaleniach dockerow mozna zakomentowac migracje baz
- na koncu wykonaja się testy serwisów

URLs:

- POST http://localhost:8123/api/wallet
- GET http://localhost:8123/api/wallet/balance/{id}
- GET http://localhost:8123/api/wallet/history/{id}
- POST http://localhost:8123/api/walletTransaction/addToWallet
- POST http://localhost:8123/api/walletTransaction/subFromWallet



