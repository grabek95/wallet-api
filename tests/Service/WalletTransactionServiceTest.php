<?php

namespace App\Tests\Service;

use App\Repository\WalletRepository;
use App\Service\WalletService;
use App\Service\WalletTransactionService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class WalletTransactionServiceTest extends KernelTestCase
{
    private $entityManager;
    private WalletTransactionService $walletTransactionService;
    private WalletRepository $walletRepository;
    private WalletService $walletService;
    private $wallet;

    protected function setUp(): void
    {
        parent::setUp();
        $kernel = self::bootKernel();
        $this->walletRepository = $kernel->getContainer()->get(WalletRepository::class);
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $this->walletTransactionService = new WalletTransactionService($this->walletRepository, $this->entityManager);
        $this->walletService = new WalletService($this->walletRepository, $this->entityManager);
        $sum = 50;
        $this->wallet = $this->walletService->create($sum);


    }

    public function testAddToWallet()
    {
        $sumToAdd = 80;
        $resultAddToWallet = $this->walletTransactionService->addToWallet($this->wallet->getId(), $sumToAdd);
        self::assertEquals($this->wallet->getSum(), $resultAddToWallet->getSum());
        self::assertEquals($this->wallet->getId(), $resultAddToWallet->getId());

    }

    public function testSubFromWallet()
    {
        $sumToSub = 30;
        $resultAddToWallet = $this->walletTransactionService->subFromWallet($this->wallet->getId(), $sumToSub);
        self::assertEquals($this->wallet->getSum(), $resultAddToWallet->getSum());
        self::assertEquals($this->wallet->getId(), $resultAddToWallet->getId());
    }
}
