<?php

namespace App\Tests\Service;

use App\Repository\StudentRepository;
use App\Repository\WalletRepository;
use App\Service\WalletService;
use App\Services\ApiServices\ApiStudentsService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class WalletServiceTest extends KernelTestCase
{
    private WalletRepository $walletRepository;
    private WalletService $walletService;

    protected function setUp(): void
    {
        parent::setUp();
        $kernel = self::bootKernel();
        $this->walletRepository = $kernel->getContainer()->get(WalletRepository::class);
        $this->walletService = new WalletService($this->walletRepository);

    }

    public function testCreate()
    {
        $sum = 50;
        $result = $this->walletService->create($sum);
        self::assertEquals($sum, $result->getSum());

    }

    public function testGetWalletWithHistoryCsvResponse()
    {
        $sum = 50;
        $wallet = $this->walletService->create($sum);
        $result = $this->walletService->getWalletWithHistoryCsvResponse($wallet->getId());
        self::assertEquals($result->getStatusCode(), 200);
    }

    public function testGetSumByWalletId()
    {
        $sum = 50;
        $wallet = $this->walletService->create($sum);
        $result = $this->walletService->getSumByWalletId($wallet->getId());
        self::assertEquals($wallet->getSum(), $result->getSum());
    }
}
